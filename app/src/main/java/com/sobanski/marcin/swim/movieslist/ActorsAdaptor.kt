package com.sobanski.marcin.swim.movieslist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.actor_list_row.view.*


open class ActorsAdaptor(private val actorsList: List<Actor>, context: Context) : BaseAdapter() {

    private val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = inflater.inflate(R.layout.actor_list_row, parent, false)

        val actor = getItem(position) as Actor

        view.actorImage.setImageResource(actor.imageResource ?: R.mipmap.ic_launcher)
        view.actorName.text = actor.name

        return view
    }

    override fun getItem(position: Int): Any {
        return actorsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return actorsList.size
    }
}