package com.sobanski.marcin.swim.movieslist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

class MovieDetailsActivity
    : AppCompatActivity(), MovieDescriptionFragment.MovieDescriptionFragmentListener,
        MoviePicturesFragment.MoviePicturesFragmentListener, CastFragment.CastFragmentListener {

    override lateinit var movie: Movie
    private val fragmentManager = supportFragmentManager
    private var isDescription: Boolean = false
    private var isLand = false

    private val descriptionFragment = fragmentManager
            .findFragmentById(R.id.activityDescriptionFrame) ?:
            MovieDescriptionFragment()
    private val pictureFragment = fragmentManager
            .findFragmentById(R.id.activityPictureFrame) ?: MoviePicturesFragment()
    private val castFragment = fragmentManager
            .findFragmentById(R.id.activityCastFrame) ?: CastFragment()

    companion object {
        const val EXTRA_RESULT_SCORE = "EXTRA_RESULT_SCORE"
        const val STATE_FRAGMENT = "fragmentState"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        isLand = resources.getBoolean(R.bool.isLand)

        movie = intent.getSerializableExtra(MoviesListActivity.EXTRA_MOVIE) as Movie


        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.activityDescriptionFrame, descriptionFragment)
        fragmentTransaction.replace(R.id.activityPictureFrame, pictureFragment)
        fragmentTransaction.replace(R.id.activityCastFrame, castFragment)
        fragmentTransaction.detach(descriptionFragment)
        fragmentTransaction.detach(pictureFragment)
        fragmentTransaction.detach(castFragment)
        fragmentTransaction.commit()






        if (savedInstanceState != null)
            restoreFragment(savedInstanceState)
        else {
            setDetailsFragment()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.movie_description_menu, menu)
        return true
    }

    override fun finish() {
        val intentResult = Intent()
        intentResult.putExtra(EXTRA_RESULT_SCORE, movie.score)
        intentResult.putExtra(MoviesListActivity.EXTRA_MOVIE_ID,
                intent.getIntExtra(MoviesListActivity.EXTRA_MOVIE_ID, -1))
        setResult(Activity.RESULT_OK, intentResult)

        super.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.movieDescriptionMenuItemDetails -> {
                    setDetailsFragment()
                }
                R.id.movieDescriptionMenuItemCast -> {
                    setCastFragment()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setDetailsFragment() {
        if (!isDescription) {
            fragmentManager.executePendingTransactions()
            val ft = fragmentManager.beginTransaction()
            ft.attach(descriptionFragment)
            ft.detach(pictureFragment)
            ft.detach(castFragment)
            ft.commit()
            isDescription = !isDescription
        }
    }


    private fun setCastFragment() {
        if (isDescription) {
            fragmentManager.executePendingTransactions()
            val ft = fragmentManager.beginTransaction()
            ft.detach(descriptionFragment)
            ft.attach(pictureFragment)
            ft.attach(castFragment)
            ft.commit()
            isDescription = !isDescription
        }
    }

    private fun restoreFragment(savedInstanceState: Bundle) {
        when (savedInstanceState.getBoolean(STATE_FRAGMENT, true)) {
            true -> {
                isDescription = false
                setDetailsFragment()
            }
            false -> {
                isDescription = true
                setCastFragment()
            }
        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(STATE_FRAGMENT, isDescription)


    }


}
