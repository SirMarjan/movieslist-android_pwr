package com.sobanski.marcin.swim.movieslist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_movies_list.*
import java.io.Serializable
import java.util.*


class MoviesListActivity : AppCompatActivity() {

    private val movieList: MutableList<Movie> = LinkedList()
    private val mAdapter = MoviesAdapter(movieList)

    companion object {
        const val EXTRA_MOVIE = "movie"
        const val EXTRA_MOVIE_ID = "movieId"
        const val REQUEST_CODE_MOVIE_DETAILS = 100
        const val STATE_MOVIES_LIST = "movieListState"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_list)


        recycler_view.layoutManager = LinearLayoutManager(applicationContext)
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recycler_view.adapter = mAdapter
        recycler_view.addOnItemTouchListener(
                RecyclerTouchListener(applicationContext, recycler_view, object : ClickListener {
            override fun onClick(view: View, position: Int) {
                val movie = movieList[position]
                val intent = Intent(applicationContext, MovieDetailsActivity::class.java)
                intent.putExtra(EXTRA_MOVIE, movie)
                intent.putExtra(EXTRA_MOVIE_ID, position)
                startActivityForResult(intent, REQUEST_CODE_MOVIE_DETAILS)
            }

            override fun onLongClick(view: View, position: Int) {
                val selectMovie = movieList[position]
                if (!selectMovie.watched)
                    Toast.makeText(applicationContext, getString(R.string.toWatchedToast),
                            Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(applicationContext, getString(R.string.toNoWatchedToast),
                            Toast.LENGTH_SHORT).show()

                selectMovie.watched = !selectMovie.watched
                mAdapter.notifyDataSetChanged()
            }
        }))

        val simpleItemTouchCallback = object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?,
                                target: RecyclerView.ViewHolder?): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                if (swipeDir == ItemTouchHelper.LEFT || swipeDir == ItemTouchHelper.RIGHT) {
                    mAdapter.removeItem(viewHolder.adapterPosition)
                    isItemViewSwipeEnabled
                }

            }
        }

        ItemTouchHelper(simpleItemTouchCallback).attachToRecyclerView(recycler_view)

        if (savedInstanceState == null) {
            prepareMovieDate()
        } else {
            val restoreMovieList =
                    savedInstanceState.getSerializable(STATE_MOVIES_LIST) as? Iterable<Movie>
                            ?: throw ClassCastException()
            movieList.addAll(restoreMovieList)
        }

        mAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_MOVIE_DETAILS ->
                if (data != null && resultCode == Activity.RESULT_OK) {
                    val position = data.getIntExtra(EXTRA_MOVIE_ID, -1)
                    if (position != -1) {
                        movieList[position].score = data
                                .getFloatExtra(MovieDetailsActivity.EXTRA_RESULT_SCORE, 3f)
                    }
                }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putSerializable(STATE_MOVIES_LIST, movieList as Serializable)
    }

    private fun prepareMovieDate() {
        var movie: Movie

        movie = Movie("Aliens", "Science Fiction", "1986",
                posterId = R.drawable.aliens,
                description = "Był sobie obcy",
                pictures = mutableListOf(
                        R.drawable.aliens_1,
                        R.drawable.aliens_2,
                        R.drawable.aliens_3,
                        R.drawable.aliens_4,
                        R.drawable.aliens_5,
                        R.drawable.alines_6),
                actors = mutableListOf(
                        Actor("Sigourney Weaver", R.drawable.aliens_a_1),
                        Actor("Michael Biehn", R.drawable.aliens_a_2),
                        Actor("Paul Reiser", R.drawable.aliens_a_3),
                        Actor("Lance Henriksen", R.drawable.aliens_a_4),
                        Actor("Carrie Henn", R.drawable.aliens_a_5)))
        movieList.add(movie)

        movie = Movie("Mad Max: Fury Road", "Action & Adventure", "2015",
                posterId = R.drawable.madmax,
                pictures = mutableListOf(
                        R.drawable.madmax_1,
                        R.drawable.madmax_2,
                        R.drawable.madmax_3,
                        R.drawable.madmax_4,
                        R.drawable.madmax_5,
                        R.drawable.madmax_6),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Inside Out", "Animation, Kids & Family", "2015",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Alien", "Horror", "1979",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Shaun the Sheep", "Animation", "2015",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("The Martian", "Science Fiction & Fantasy", "2015",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Mission: Impossible", "Action", "1996",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Up", "Animation", "2009",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Star Trek", "Science Fiction", "2009",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("The LEGO Movie", "Animation", "2014",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Iron Man", "Action & Adventure", "2008",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Chicken Run", "Animation", "2000",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Back to the Future", "Science Fiction", "1985",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Raiders of the Lost Ark", "Action & Adventure", "1981",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Goldfinger", "Action & Adventure", "1965",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)

        movie = Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014",
                pictures = mutableListOf(null, null, null, null, null, null),
                actors = mutableListOf())
        movieList.add(movie)
    }

}
