package com.sobanski.marcin.swim.movieslist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.movie_list_row_even.view.*
import kotlinx.android.synthetic.main.movie_list_row_odd.view.*


class MoviesAdapter(private val moviesList: MutableList<Movie>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ODD_VIEW: Int = 101
        private const val EVEN_VIEW: Int = 102
    }


    abstract inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract val poster: ImageView
        abstract val title: TextView
        abstract val year: TextView
        abstract val genre: TextView
        abstract val watched: ImageView
    }

    inner class MyViewHolderEven(view: View) : MyViewHolder(view) {

        override val poster: ImageView = view.rowMoviePosterEven
        override val title: TextView = view.rowMovieTitleEven
        override val year: TextView = view.rowMovieYearEven
        override val genre: TextView = view.rowMovieGenreEven
        override val watched: ImageView = view.rowMovieWatchedEven

    }

    inner class MyViewHolderOdd(view: View) : MyViewHolder(view) {

        override val poster: ImageView = view.rowMoviePosterOdd
        override val title: TextView = view.rowMovieTitleOdd
        override val year: TextView = view.rowMovieYearOdd
        override val genre: TextView = view.rowMovieGenreOdd
        override val watched: ImageView = view.rowMovieWatchedOdd


    }

    override fun getItemViewType(position: Int): Int {
        val viewType: Int
        if (position % 2 == 0) {
            viewType = EVEN_VIEW
        } else {
            viewType = ODD_VIEW
        }

        return viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewHolder: MyViewHolder
        val inflater = LayoutInflater.from(parent.context)
        val view: View

        when (viewType) {
            EVEN_VIEW -> {
                view = inflater.inflate(R.layout.movie_list_row_even, parent, false)
                viewHolder = MyViewHolderEven(view)
            }
            ODD_VIEW -> {
                view = inflater.inflate(R.layout.movie_list_row_odd, parent, false)
                viewHolder = MyViewHolderOdd(view)
            }
            else -> {
                throw IllegalArgumentException()
            }
        }

        return viewHolder
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = moviesList[position]
        val myViewHolder = holder as MyViewHolder
        myViewHolder.poster.setImageResource(movie.posterId)
        myViewHolder.title.text = movie.title
        myViewHolder.genre.text = movie.genre
        myViewHolder.year.text = movie.year
        myViewHolder.watched.visibility = if (movie.watched) View.VISIBLE else View.GONE

    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    fun removeItem(position: Int) {
        moviesList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, moviesList.size)
    }
}
