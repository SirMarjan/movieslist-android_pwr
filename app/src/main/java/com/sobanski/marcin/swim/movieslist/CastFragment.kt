package com.sobanski.marcin.swim.movieslist

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_movie_cast.*


class CastFragment : Fragment() {

    private lateinit var listener: CastFragmentListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? CastFragmentListener ?:
                throw ClassCastException(context.toString() + " must implemented interface")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_movie_cast, container, false) ?:
                throw Exception("Inflater error")

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val listView = movieCastListView
        val actorList: List<Actor> = listener.movie.actors

        listView.adapter = ActorsAdaptor(actorList, context)

    }

    interface CastFragmentListener {
        val movie: Movie
    }

}
