package com.sobanski.marcin.swim.movieslist

import java.io.Serializable

class Actor(val name: String = "Al Pacino", val imageResource: Int? = null) : Serializable