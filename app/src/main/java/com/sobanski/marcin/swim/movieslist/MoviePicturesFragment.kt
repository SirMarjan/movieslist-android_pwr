package com.sobanski.marcin.swim.movieslist


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_movie_pictures.*


class MoviePicturesFragment : Fragment() {
    private lateinit var listener: MoviePicturesFragmentListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? MoviePicturesFragmentListener ?:
                throw ClassCastException(context.toString() + " must implemented interface")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_movie_pictures, container, false) ?:
                throw Exception("Inflater error")

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val pictureList = listener.movie.pictures

        moviePicture_1.setImageResource(pictureList[0] ?: R.mipmap.ic_launcher)
        moviePicture_2.setImageResource(pictureList[1] ?: R.mipmap.ic_launcher)
        moviePicture_3.setImageResource(pictureList[2] ?: R.mipmap.ic_launcher)
        moviePicture_4.setImageResource(pictureList[3] ?: R.mipmap.ic_launcher)
        moviePicture_5.setImageResource(pictureList[4] ?: R.mipmap.ic_launcher)
        moviePicture_6.setImageResource(pictureList[5] ?: R.mipmap.ic_launcher)
    }

    interface MoviePicturesFragmentListener {
        val movie: Movie
    }
}