package com.sobanski.marcin.swim.movieslist

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import kotlinx.android.synthetic.main.fragment_movie_description.*


class MovieDescriptionFragment : Fragment() {
    private lateinit var listener: MovieDescriptionFragmentListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? MovieDescriptionFragmentListener ?:
                throw ClassCastException(context.toString() + " must implemented interface")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_movie_description, container, false) ?:
                throw Exception("Inflater error")

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val movie = listener.movie
        detailsImage.setImageResource(movie.pictures.first() ?: movie.posterId)
        detailsTitle.text = movie.title
        detailsDescription.text = movie.description
        detailsRatingBar.rating = movie.score

        detailsRatingBar.onRatingBarChangeListener = RatingBar
                .OnRatingBarChangeListener { _, rating, _ -> listener.movie.score = rating }
    }

    interface MovieDescriptionFragmentListener {
        val movie: Movie
    }

}