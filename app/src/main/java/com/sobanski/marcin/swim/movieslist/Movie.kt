package com.sobanski.marcin.swim.movieslist

import java.io.Serializable

class Movie(val title: String,
            val genre: String,
            val year: String,
            val description: String = "Desc",
            val posterId: Int = R.mipmap.ic_launcher, var score: Float = 3f,
            val pictures: MutableList<Int?>,
            val actors: MutableList<Actor>,
            var watched: Boolean = false) : Serializable