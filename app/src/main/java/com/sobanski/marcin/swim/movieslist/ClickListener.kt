package com.sobanski.marcin.swim.movieslist

import android.view.View

interface ClickListener {
    fun onClick(view: View, position: Int): Unit
    fun onLongClick(view: View, position: Int): Unit
}